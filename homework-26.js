"use strict"

// Упражнение 1
console.log('Упражнение 1:')
let arr1 = [1, 2, 10, 5];
let arr2 = ["a", {}, 3, 3, -2];

function getSumm(arr) {
  let summ = 0;
  let onlyNumber = arr.filter(function (n) {
    return n === +n;
  });
  console.log(onlyNumber);
  for (let i = 0; i < onlyNumber.length; i++) {
    summ += onlyNumber[i];
  }
  return summ;
}
console.log(getSumm(arr2));

// Упражнение 2 в data.js
console.log('Упражнение 2 в data.js')

// Упражнение 3
console.log('Упражнение 3:')

let cart = [4884];

function addToCart(id) {
  if (cart.includes(id) === false) {
    cart.push(id);
    console.log(cart);
  }
}

function removeFromCart(id) {
  cart = cart.filter(function (n) {
    return n !== id;
  })
  console.log(cart);
}

addToCart(3456);
addToCart(1);
addToCart(2);
addToCart(1);
addToCart(2);
addToCart(3456);
addToCart(4884);
addToCart('artem');
addToCart('artem');
addToCart(4884);

removeFromCart(3456);
removeFromCart(2);
removeFromCart('artem');