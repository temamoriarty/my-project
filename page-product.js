'use strict'

let form = document.querySelector('.form');

let inputNameContainer = form.querySelector('.add-opinion__name');
let inputRatingContainer = form.querySelector('.add-opinion__rating')

let inputName = inputNameContainer.querySelector('.input-name');
let inputNameError = inputNameContainer.querySelector('.error');

let inputRating = inputRatingContainer.querySelector('.input-rating');
let inputRatingError = inputRatingContainer.querySelector('.error');

let textArea = form.querySelector('.text-opinion');

let header = document.querySelector('.header');
let sidebar = document.querySelector('.sidebar');

let countCart = header.querySelector('.header__count-cart');
let addBtn = sidebar.querySelector('.purchase__button');


window.addEventListener('DOMContentLoaded', () => {
  // Переменные для отзыва

  let textOpinion = localStorage.getItem('opinion');
  let name = localStorage.getItem('name');
  let rating = localStorage.getItem('rating');

  inputName.value = name;
  inputRating.value = rating;
  textArea.value = textOpinion;

  // Переменные для функционала корзины
  let count = localStorage.getItem('cartInfo');
  let background = localStorage.getItem('background');
  let textBtn = localStorage.getItem('addBtn');

  if (localStorage.length !== 0) {
    countCart.innerHTML = count;
    addBtn.style.background = background
    addBtn.innerHTML = textBtn;
    countCart.classList.toggle('hide', +count === 0);
  }
});

// Функционал для корзины

addBtn.addEventListener('click', () => {
  let count = +localStorage.getItem('cartInfo');

  if (count === 0) {
    addToCart()
  } else {
    removeToCart()
  }
})


function addToCart() {
  localStorage.setItem('cartInfo', 1);
  countCart.innerHTML = localStorage.getItem('cartInfo');
  addBtn.style.background = '#888888';
  localStorage.setItem('background', addBtn.style.background);
  addBtn.innerHTML = 'Товар уже в корзине';
  localStorage.setItem('addBtn', addBtn.innerHTML);
  countCart.classList.toggle('hide');
}

function removeToCart() {
  localStorage.setItem('cartInfo', 0);
  countCart.innerHTML = localStorage.getItem('cartInfo');
  addBtn.style.background = '';
  localStorage.setItem('background', addBtn.style.background);
  addBtn.innerHTML = 'Добавить в корзину';
  localStorage.setItem('addBtn', addBtn.innerHTML);
  countCart.classList.toggle('hide');
}

function handleSubmit(event) {

  event.preventDefault();
  let errorName = "";
  let errorRating = "";
  let name = inputName.value;
  let rating = +inputRating.value;

  console.log('Submit');


  if (name.length === 0) {
    errorName = 'Вы забыли указать имя и фамилию';
  } else if (name.length < 2) {
    errorName = 'Имя не может быть короче 2-х символов';
  } else errorName = '';

  inputNameError.innerText = errorName;

  if (errorName) {
    inputNameError.classList.add("error_visiable");
    inputName.classList.add("input-error");
    return console.log('Обнаружена ошибка! Дальше проверяться не буду!');
  } else {
    inputNameError.classList.remove("error_visiable");
    inputName.classList.remove("input-error");
  }

  if (rating.length === 0 || rating < 1 || rating > 5 || isNaN(rating)) {
    errorRating = 'Оценка должна быть от 1 до 5';
  } else {
    errorRating = '';
  }

  inputRatingError.innerText = errorRating;

  if (errorRating) {
    inputRatingError.classList.add("error_visiable");
    inputRating.classList.add("input-error");
  } else {
    inputRatingError.classList.remove("error_visiable");
    inputRating.classList.remove("input-error");
    localStorage.removeItem('name');
    inputName.value = '';
    localStorage.removeItem('rating');
    inputRating.value = '';
    localStorage.removeItem('opinion');
    textArea.value = '';
  }

};

function handleClick(event) {
  inputNameError.classList.remove("error_visiable");
  inputName.classList.remove("input-error");
  inputRatingError.classList.remove("error_visiable");
  inputRating.classList.remove("input-error");
}

function handleStorage(event) {
  inputName.value = localStorage.getItem('name');
  inputRating.value = localStorage.getItem('rating');
  textArea.value = localStorage.getItem('opinion');
}

function handleInput(e) {
  localStorage.setItem(e.target.name, e.target.value);
}



form.addEventListener('submit', handleSubmit);
form.addEventListener('click', handleClick);

form.addEventListener('input', handleInput);
window.addEventListener('storage', handleStorage);






