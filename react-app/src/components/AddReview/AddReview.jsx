import React from "react";
import FormReview from "../FormReview/FormReview";
import styled from "styled-components";

const AddReviewTag = styled.section`
  padding-left: 170px;
  box-sizing: border-box;
  @media (max-width: 1023px) {
    padding-left: 0px;
  }
`

function AddReview() {
  return (
    <AddReviewTag>
      <FormReview title='Добавить свой отзыв'>
      </FormReview>
    </AddReviewTag>
  )
}

export default AddReview;