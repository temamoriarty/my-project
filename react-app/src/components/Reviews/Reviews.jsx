import React from "react";
import reviews from "../../data/reviews";
import Review from "../Review/Review";
import './Reviews.css';


function Reviews(props) {

  return (
    <div className="review-section__list">
      {reviews.map((review) => {
        return (
          <Review
            key={review.id}
            id={review.id}
            userPhoto={review.userPhoto}
            userName={review.userName}
            alt={review.alt}
            rating={review.rating}
            yellowStar={review.yellowStar}
            grayStar={review.grayStar}
            altYellowStar={review.altYellowStar}
            altGrayStar={review.altGrayStar}
            experience={review.experience}
            advantages={review.advantages}
            disadvantages={review.disadvantages}
          />
        )
      })}
    </div>
  )
}

export default Reviews;