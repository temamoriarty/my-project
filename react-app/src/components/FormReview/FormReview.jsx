import React, { useState } from "react";
import ButtonOpinion from "../ButtonOpinion/ButtonOpinion";
import Field from "../Field/Field";
import TextOpinion from "../TextOpinion/TextOpinion";
import './FormReview.css';

function FormReview(props) {
  const { title } = props;
  const initialValues = {
    name: localStorage.getItem('name') ? localStorage.getItem('name') : '',
    rating: localStorage.getItem('rating') ? localStorage.getItem('rating') : '',
    opinion: localStorage.getItem('opinion') ? localStorage.getItem('opinion') : ''
  }
  const [formValues, setFormValues] = useState(initialValues)
  const [error, setError] = useState({})

  const handleChange = (e) => {
    const { name, value } = e.target
    setFormValues({ ...formValues, [name]: value })
    localStorage.setItem(e.target.name, e.target.value)
  }

  const validate = (values) => {
    if (values.name.length === 0) {
      return {
        text: 'Вы забыли указать имя и фамилию',
        key: 'name'
      };
    } else if (values.name.length < 2) {
      return {
        text: 'Имя не может быть короче 2-х символов',
        key: 'name'
      }
    } else if
      (values.rating.length === 0 || +values.rating < 1 || +values.rating > 5 || isNaN(+values.rating)) {
      return {
        text: 'Оценка должна быть от 1 до 5',
        key: 'rating'
      }
    } else return null
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log('Submit!')
    const error = validate(formValues);

    if (error) {
      setError(error);
    } else {
      setFormValues({ name: '', rating: '', opinion: '' })
      localStorage.removeItem('name');
      localStorage.removeItem('rating');
      localStorage.removeItem('opinion');
      setError({});
    }

  }

  return (
    <form onSubmit={handleSubmit} className="form-review">
      <div className="form-review__title">{title}</div>
      <div className="review-content">
        <div className="required-form">
          <Field
            title={'name'}
            placeholder={'Имя и фамилия'}
            onChange={handleChange}
            value={formValues.name}
            error={error.key === 'name' && error.text}
          />
          <Field
            title={'rating'}
            placeholder={'Оценка'}
            onChange={handleChange}
            value={formValues.rating}
            error={error.key === 'rating' && error.text}
          />
        </div>
        <TextOpinion
          title={'opinion'}
          placeholder={'Текст отзыва'}
          onChange={handleChange}
          value={formValues.opinion}
        />
        <ButtonOpinion />
      </div>
    </form>
  )
}

export default FormReview;