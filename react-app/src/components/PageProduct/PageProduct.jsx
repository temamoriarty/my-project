import React from "react"; // Используется React, чтоб создать компонент
import AddReview from "../AddReview/AddReview";
import Breadcrumbs from "../Breadcrumbs/Breadcrumbs";
import Colors from "../Colors/Colors";
import Configs from "../Configs/Configs";
import Footer from "../Footer/Footer";
import Header from "../Header/Header";
import Link from "../Link/Link";
import Models from "../Models/Models";
import Reviews from "../Reviews/Reviews";
import Sidebar from "../Sidebar/Sidebar";
import './PageProduct.css';

const breadcrumbs = [
  {
    href: '#',
    text: 'Электроника'
  },
  {
    href: '#',
    text: 'Смартфоны и гаджеты'
  },
  {
    href: '#',
    text: 'Мобильные телефоны'
  },
  {
    href: '#',
    text: 'Apple'
  }
];

function PageProduct(props) {
  return (
    <div className="page-product">
      <Header />

      <div className="content" id="up">
        <Breadcrumbs items={breadcrumbs} />

        <main className="main">
          <h2 className="product-name main__product-name">Смартфон Apple iPhone 13</h2>
          <div className="product-images main__product-images">
            <img className="product-images__image" src="../images/image-1.webp" alt="Blue IPhone" />
            <img className="product-images__image" src="../images/image-2.webp" alt="Blue IPhone" />
            <img className="product-images__image" src="../images/image-3.webp" alt="Blue IPhone" />
            <img className="product-images__image" src="../images/image-4.webp" alt="Blue IPhone" />
            <img className="product-images__image" src="../images/image-5.webp" alt="Blue IPhone" />
          </div>

          <div className="product-card">
            <div className="product-information">
              <section className="product-color">
                <Colors />
              </section>

              <section className="memory">
                <Configs />
              </section>

              <section className="characteristic">
                <h3 className="product-information__title-characteristic">Характеристики
                  товара</h3>
                <ul className="characteristic__list">
                  <li className="characteristic__point">Экран: <strong>6.1</strong></li>
                  <li className="characteristic__point">Встроенная память: <strong>128 ГБ</strong></li>
                  <li className="characteristic__point">Операционная система: <strong>iOS 15</strong></li>
                  <li className="characteristic__point">Беспроводные интерфейсы: <strong>NFC, Bluetooth,
                    Wi-Fi</strong></li>
                  <li className="characteristic__point">Процессор: <strong>
                    <Link href="https://ru.wikipedia.org/wiki/Apple_A15" target="_blank" rel="noreferrer">Apple A15
                      Bionic</Link>
                  </strong>
                  </li>
                  <li className="characteristic__point">Вес: <strong>173 г</strong></li>
                </ul>
              </section>

              <section className="description">
                <h3 className="product-information__title-characteristic">Описание</h3>
                <div className="description__text">
                  <p className="description__text-paragraph">
                    Наша самая совершенная система двух камер.<br />
                    Особый взгляд на прочность дисплея.<br />
                    Чип, с которым всё супербыстро.<br />
                    Аккумулятор держится заметно дольше.<br />
                    <i>iPhone 13 - сильный мира всего.</i>
                  </p>
                  <p className="description__text-paragraph">
                    Мы разработали совершенно новую схему расположения и развернули
                    объективы на 45 градусов. Благодаря этому внутри корпуса поместилась наша
                    лучшая система двух камер с увеличенной матрицей широкоугольной камеры.
                    Кроме того, мы освободили место для системы оптической стабилизации
                    изображения сдвигом матрицы. И повысили скорость работы матрицы на
                    сверхширокоугольной камере.
                  </p>
                  <p className="description__text-paragraph">
                    Новая сверхширокоугольная камера видит больше деталей в тёмных областях
                    снимков. Новая <br /> широкоугольная камера улавливает на 47% больше света для
                    более качественных фотографий и видео. <br /> Новая оптическая стабилизация со
                    сдвигом матрицы обеспечит чёткие кадры даже в неустойчивом <br /> положении.
                  </p>
                  <p className="description__text-paragraph">
                    Режим «Киноэффект» автоматически добавляет великолепные эффекты перемещения
                    фокуса и изменения резкости. Просто начните запись видео. Режим «Киноэффект»
                    будет удерживать фокус на объекте съёмки, создавая красивый эффект размытия
                    вокруг него. Режим «Киноэффект» распознаёт, когда нужно перевести фокус на другого
                    человека или объект, который появился в кадре. Теперь ваши видео будут смотреться
                    как настоящее кино.
                  </p>
                </div>
              </section>

              <Models />

              <section className="review-section product-information__review-section">
                <div className="review-section__header">
                  <div className="review-section__wrapper">
                    <h3 className="review-section__title">Отзывы</h3>
                    <span className="review-section__count">425</span>
                  </div>
                </div>
                <Reviews />
              </section>

              <AddReview />
            </div>

            <Sidebar product={{ id: 228, name: 'Iphone 13' }} />
          </div>
        </main>
      </div>
      <Footer />
    </div>
  );
}

// Экспортируем компонент, чтобы его можно было использовать

export default PageProduct;