import React from "react";
import { useCurrentDate } from "@kundinos/react-hooks";
import Link from "../Link/Link";

function Footer() {

  const currentDate = useCurrentDate();
  const fullYear = currentDate.getFullYear();


  return (
    <footer className="footer footer_indent-top">
      <div className="content">
        <div className="footer-content">
          <div className="contacts">
            <span className="contacts__title">
              © ООО «<span className="first-word">Мой</span>Маркет», 2018-{fullYear}.
            </span>
            <p className="contacts__text">
              Для уточненияинформации звоните по номеру <Link href="tel:79000000000">+7 900 000
                0000</Link>, а предложения по сотрудничеству отправляйте на почту <Link
                  href="mailto:partner@mymarket.com">partner@mymarket.com</Link>
            </p>
          </div>
          <Link href="#up">Наверх</Link>
        </div>
      </div>
    </footer>
  )
}

export default Footer;