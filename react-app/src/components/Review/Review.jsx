import React from "react";
import './Review.css'


function Review({
  id,
  userPhoto,
  alt,
  userName,
  rating,
  yellowStar,
  altYellowStar,
  grayStar,
  altGrayStar,
  experience,
  advantages,
  disadvantages }) {
  return (
    <>
      <div className="review">
        <img src={userPhoto} alt={alt} className="review__photo" />
        <div className="review__content">
          <h4 className="review__name">{userName}</h4>
          <div className="review__rating">
            {rating === 5 && <><img src={yellowStar} alt={altYellowStar} className="star" />
              <img src={yellowStar} alt={altYellowStar} className="star" />
              <img src={yellowStar} alt={altYellowStar} className="star" />
              <img src={yellowStar} alt={altYellowStar} className="star" />
              <img src={yellowStar} alt={altYellowStar} className="star" />
            </>
            }
            {rating === 4 && <><img src={yellowStar} alt={altYellowStar} className="star" />
              <img src={yellowStar} alt={altYellowStar} className="star" />
              <img src={yellowStar} alt={altYellowStar} className="star" />
              <img src={yellowStar} alt={altYellowStar} className="star" />
              <img src={grayStar} alt={altGrayStar} className="star" />
            </>
            }
          </div>
          <div className="review__parameter-list">
            <div className="review__parameter">
              <p className="review__paragraph"><span className="text-bold">Опыт
                использования:</span>
                {experience}</p>
            </div>
            <div className="review__parameter">
              <span className="text-bold">Достоинства:</span>
              <p className="review__paragraph">
                {advantages}
              </p>
            </div>
            <div className="review__parameter">
              <span className="text-bold">Недостатки:</span>
              <p className="review__paragraph">
                {disadvantages}
              </p>
            </div>
          </div>
        </div>
      </div>
      {id < 2 && <div className="separator review-section__separator"></div>}
    </>
  )
}

export default Review;