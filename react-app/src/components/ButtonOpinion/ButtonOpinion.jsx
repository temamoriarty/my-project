import React from "react";
import './ButtonOpinion.css';

function ButtonOpinion() {
  return (
    <div className="add-review__button">
      <button className="button-opinion form__button-opinion" type="submit">
        Отправить отзыв
      </button>
    </div>
  )
}

export default ButtonOpinion;