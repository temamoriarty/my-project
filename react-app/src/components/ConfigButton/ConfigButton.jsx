import React from "react";
import './ConfigButton.css';

function ConfigButton({ memorySize, selected, ...restProps }) {
  const className = `config-button ${selected ? 'config-button_selected' : ''}`;
  return (
    <button {...restProps} className={className}>{memorySize} ГБ</button>
  )
}

export default ConfigButton;