import React from "react";
import { Link } from 'react-router-dom';
import styles from './MainIndex.module.css';

function MainIndex() {
  return (
    <div className={styles.content} id="up">
      <main className={styles.main}>
        <div className={styles.main__content}>
          Здесь должно быть содержимое главной страницы.
          Но в рамках курса главная страница  используется лишь
          в демонстрационных целях
        </div>
        <div className="link">
          <Link to='/product'>Перейти на страницу товара</Link>
        </div>
      </main>
    </div>
  )
}

export default MainIndex;