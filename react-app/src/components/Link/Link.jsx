import React from "react";
import './Link.css';

function Link({ href, children, ...props }) {
  return (
    <a {...props} className="link" href={href}>{children}</a>
  )
}

export default Link;