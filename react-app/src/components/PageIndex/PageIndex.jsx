import React from 'react';
import Footer from '../Footer/Footer';
import Header from '../Header/Header';
import MainIndex from '../MainIndex/MainIndex';
import './PageIndex.css'

function PageIndex() {
  return (
    <div className='page-index'>
      <Header />
      <MainIndex />
      <Footer />
    </div>
  )
}

export default PageIndex;