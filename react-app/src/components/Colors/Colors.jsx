import React, { useState } from "react";
import colors from "../../data/colors";
import ColorButton from "../ColorButton/ColorButton";
import './Colors.css';

function Colors(props) {
  const [selectedColor, setSelectedColor] = useState(4)
  const handleClick = (e, id) => {
    setSelectedColor(id);
  }
  return (
    <>
      <h3 className="product-information__title-characteristic">Цвет товара:
        {' ' + colors.find(color => color.id === selectedColor).alt}</h3>
      <div className="colors">
        {colors.map((color) => {
          return (
            <ColorButton
              image={color.image}
              alt={color.alt}
              key={color.id}
              selected={color.id === selectedColor}
              onClick={(e) => handleClick(e, color.id)}
            />
          )
        })}
      </div>
    </>

  )
}

export default Colors;