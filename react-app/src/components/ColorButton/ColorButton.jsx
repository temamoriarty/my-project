import React from "react";
import "./ColorButton.css";

function ColorButton({ image, alt, selected, ...restProps }) {
  const className = `color-button ${selected ? 'color-button_selected' : ''}`
  return (
    <div {...restProps} className={className}>
      <img src={image} alt={alt} />
    </div>
  )
}

export default ColorButton;