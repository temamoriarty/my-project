import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { addProduct, removeProduct } from "../../reducers/cart-reducer";
import { addToFavorites, removeFromFavorites } from "../../reducers/favorites-reducer";
import './Sidebar.css';

function Sidebar({ product }) {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.cart);
  const favorites = useSelector((state) => state.favorites);

  const hasInCart = cart.products.some((item) => {
    return item.id === product.id;
  });

  const hasInFavorites = favorites.products.some((item) => {
    return item.id === product.id;
  });

  const handleClickAddToCart = (product) => {
    if (!hasInCart) {
      dispatch(addProduct(product));
    } else {
      dispatch(removeProduct(product));
    }
  }

  const handleClickAddToFavorites = (product) => {
    if (!hasInFavorites) {
      dispatch(addToFavorites(product));
    } else {
      dispatch(removeFromFavorites(product));
    }
  }

  return (
    <div className="sidebar">
      <div className="purchase">
        <div className="purchase-header">
          <div className="purchase__price">
            <div className="purchase__old-price">
              <span className="purchase__old-price-value">75 990₽</span>
              <div className="purchase__discount">
                <span className="purchase__discount-value">-8%</span>
              </div>
            </div>
            <span className="purchase__new-price">67 990₽</span>
          </div>
          <button className="purchase__favorites" title={
            hasInFavorites
              ? 'Удалить из избранного'
              : 'Добавить в избранное'
          } onClick={() => handleClickAddToFavorites(product)}>
            <svg className={`purchase__heart${hasInFavorites ? '-active' : ''}`} width="44" height="35" viewBox="0 0 44 35" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path className="purchase__heart-path" d="M3.30841 2.95447C7.29791 -0.875449 13.7444 -0.875449 17.7339 2.95447L22.0002 7.05027L26.2667 2.95447C30.2563 -0.875449 36.7027 -0.875449 40.6923 2.95447C44.6817 6.78439 44.6817 12.973 40.6923 16.803L22.0002 34.7472L3.30841 16.803C-0.681091 12.973 -0.681091 6.78439 3.30841 2.95447ZM14.7876 5.78289C12.4253 3.51507 8.61701 3.51507 6.25468 5.78289C3.89237 8.05071 3.89237 11.7067 6.25468 13.9746L22.0002 29.0904L37.7461 13.9746C40.1084 11.7067 40.1084 8.05071 37.7461 5.78289C35.3838 3.51507 31.5755 3.51507 29.2132 5.78289L22.0002 12.7072L14.7876 5.78289Z" />
              <path className="purchase__heart-fill" d="M6 5.5L22 21L38 5.5" />
            </svg>
          </button>
        </div>

        <div className="purchase__delivery">
          <span className="purchase__delivery-pickup">
            Самовывоз в четверг, 1 сентября — <span className="text-bold">бесплатно</span>
          </span>
          <span className="purchase__delivery-courier">
            Курьером в четверг, 1 сентября — <span className="text-bold">бесплатно</span>
          </span>
        </div>

        <button className={`purchase-button${hasInCart ? '_added' : ''}`} onClick={() => handleClickAddToCart(product)}>{hasInCart ? 'Товар уже в корзине' : 'Добавить в корзину'}</button>

      </div>
      <div className="ads">
        <div className="ads__signature">Реклама</div>
        <div className="ads__list">
          <iframe className="iframe" title="ads" src="../ads/ads.html"></iframe>
          <iframe className="iframe" title="ads" src="../ads/ads.html"></iframe>
        </div>
      </div>
    </div>
  )
}

export default Sidebar;