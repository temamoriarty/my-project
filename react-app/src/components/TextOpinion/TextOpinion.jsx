import React from "react";
import './TextOpinion.css';

function TextOpinion({ title, placeholder, onChange, value }) {
  return (
    <textarea className="text-opinion form__text-opinion" id={title} name={title}
      placeholder={placeholder} onChange={onChange} value={value}></textarea>
  )
}

export default TextOpinion;