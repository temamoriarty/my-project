import React from "react";
import './Field.css';

function Field({ title, placeholder, onChange, value, error }) {
  return (
    <div className={`field-${title}`}>
      <input className={`input-${title}`} id={title} name={title}
        placeholder={placeholder} onChange={onChange} value={value} />
      <div className={`error ${error ? 'error_visiable' : ''}`}>
        {error}
      </div>
    </div>
  )
}

export default Field;