import React from "react";

function Models() {
  return (
    <section className="models">
      <h3 className="product-information__title-characteristic">Сравнение моделей</h3>
      <table className="table">
        <thead className="table__head">
          <tr className="table__row">
            <th className="table__column">Модель</th>
            <th className="table__column">Вес</th>
            <th className="table__column">Высота</th>
            <th className="table__column">Ширина</th>
            <th className="table__column">Толщина</th>
            <th className="table__column">Чип</th>
            <th className="table__column">Объем памяти</th>
            <th className="table__column">Аккумулятор</th>
          </tr>
        </thead>
        <tbody>
          <tr className="table__row">
            <td className="table__column">Iphone 11</td>
            <td className="table__column">194 грамма</td>
            <td className="table__column">150,9 мм</td>
            <td className="table__column">75,7 мм</td>
            <td className="table__column">8,3 мм</td>
            <td className="table__column">А13 Bionic chip</td>
            <td className="table__column">до 128 Гб</td>
            <td className="table__column">До 17 часов</td>
          </tr>
          <tr className="table__row">
            <td className="table__column">Iphone 12</td>
            <td className="table__column">164 грамма</td>
            <td className="table__column">146,7 мм</td>
            <td className="table__column">71,5 мм</td>
            <td className="table__column">7,4 мм</td>
            <td className="table__column">А14 Bionic chip</td>
            <td className="table__column">до 256 Гб</td>
            <td className="table__column">До 19 часов</td>
          </tr>
          <tr className="table__row">
            <td className="table__column">Iphone 13</td>
            <td className="table__column">174 грамма</td>
            <td className="table__column">146,7 мм</td>
            <td className="table__column">71,5 мм</td>
            <td className="table__column">7,65 мм</td>
            <td className="table__column">А15 Bionic chip</td>
            <td className="table__column">до 512 Гб</td>
            <td className="table__column">До 19 часов</td>
          </tr>
        </tbody>
      </table>
    </section>
  )
}

export default Models;