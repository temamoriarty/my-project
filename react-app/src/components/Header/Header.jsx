import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom"
import './Header.css'

function Header() {
  const cart = useSelector((state) => state.cart);
  const favorites = useSelector((state) => state.favorites)

  return (
    <header className="header">
      <div className="content">
        <div className="header-wrapper">
          <Link to='/' className="header-link">
            <div className="header-logo">

              <img className="header__favicon" src="./favicon.svg" alt="logo" />
              <h1 className="title header__title"><span className="first-word">Мой</span>Маркет</h1>

            </div>
          </Link>
          <div className="header-panel">
            <div className="header-icon">
              <img className="heart" src="../images/header-heart.svg" alt="Избранное" />
              {favorites.products.length !== 0 ? (<span className="header__count-heart">{favorites.products.length}</span>) : null}
            </div>
            <div className="header-icon">
              <img className="cart" src="../images/header-cart.svg" alt="Корзина" />
              {cart.products.length !== 0 ? (<span className="header__count-cart"> {cart.products.length} </span>) : null}

            </div>
          </div>
        </div>
      </div>
    </header>
  )
}

export default Header;