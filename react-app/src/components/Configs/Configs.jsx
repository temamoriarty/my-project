import React, { useState } from "react";
import configs from "../../data/configs";
import ConfigButton from "../ConfigButton/ConfigButton";
import './Configs.css';

function Configs() {
  const [selectedConfig, setSelectedConfig] = useState(1);
  const handleClick = (e, id) => {
    setSelectedConfig(id)
  }
  return (
    <>
      <h3 className="product-information__title-characteristic">Конфигурация
        памяти: {configs.find(config => config.id === selectedConfig).memorySize} ГБ</h3>
      <div className="configs">
        {configs.map((config) => (
          <ConfigButton
            memorySize={config.memorySize}
            key={config.id}
            selected={config.id === selectedConfig}
            onClick={(e) => handleClick(e, config.id)}
          />
        ))}
      </div>
    </>
  )
}

export default Configs;