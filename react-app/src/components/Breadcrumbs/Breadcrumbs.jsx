import React from "react";
import Link from "../Link/Link";
import './Breadcrumbs.css';


function Breadcrumbs({ items }) {
  return (
    <nav className="breadcrumbs content__breadcrumbs">
      {items.map((item, index) =>
        <>
          <Link href={item.href} key={index}>{item.text}</Link>
          {index !== items.length - 1 ? '>' : null}
        </>
      )}
    </nav>
  )
}

export default Breadcrumbs;