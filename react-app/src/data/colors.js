const colors = [
  {
    image: '../images/color-1.png',
    alt: 'Красный',
    id: 1
  },
  {
    image: '../images/color-2.png',
    alt: 'Зеленый',
    id: 2
  },
  {
    image: '../images/color-3.png',
    alt: 'Розовый',
    id: 3
  },
  {
    image: '../images/color-4.png',
    alt: 'Синий',
    id: 4
  },
  {
    image: '../images/color-5.png',
    alt: 'Белый',
    id: 5
  },
  {
    image: '../images/color-6.png',
    alt: 'Черный',
    id: 6
  },
];

export default colors;