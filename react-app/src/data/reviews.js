const reviews = [
  {
    id: 1,
    userPhoto: '../images/author-1.png',
    alt: 'user',
    userName: 'Марк Г.',
    rating: 5,
    yellowStar: '../images/yellow-star.svg',
    altYellowStar: 'yellow-star',
    altGrayStar: 'gray-star',
    grayStar: '../images/gray-star.svg',
    experience: 'менее месяца',
    advantages: `это мой первый айфон после после огромного количества телефонов на андроиде.
    всё плавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая, ширик тоже на высоте.`,
    disadvantages: `к самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь)
    а если нужно переносить фото с компа, то это только через itunes, который урезает качество фотографий исходное`
  },
  {
    id: 2,
    userPhoto: '../images/author-2.png',
    alt: 'user',
    userName: 'Валерий Коваленко',
    rating: 4,
    yellowStar: '../images/yellow-star.svg',
    grayStar: '../images/gray-star.svg',
    experience: 'менее месяца',
    advantages: `OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго`,
    disadvantages: `Плохая ремонтопригодность`,
  }
]

export default reviews;