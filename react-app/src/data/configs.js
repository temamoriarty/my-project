const configs = [
  {
    memorySize: 128,
    id: 1
  },
  {
    memorySize: 256,
    id: 2
  },
  {
    memorySize: 512,
    id: 3
  }
]

export default configs;