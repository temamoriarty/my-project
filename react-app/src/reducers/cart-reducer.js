import { createSlice } from "@reduxjs/toolkit";

const initialState = { products: localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [] }

export const cartSlice = createSlice({
  name: "cart",

  // Начальное состояние хранилища, товаров нет
  initialState: initialState,

  // Все доступные метода
  reducers: {
    // Добавить товар, первый параметр это текущее состояние
    // А второй параметр имеет данные для действия
    addProduct: (prevState, action) => {
      const product = action.payload;
      const hasInCart = prevState.products.some(
        (prevProduct) => prevProduct.id === product.id
      );

      if (hasInCart) return prevState;

      localStorage.setItem('cart', JSON.stringify([...prevState.products, product]))

      return {
        ...prevState,
        // Внутри action.payload информация о добавленном товаре
        // Возвращаем новый массив товаров вместе с добавленным
        products: [...prevState.products, product],
      };
    },

    removeProduct: (prevState, action) => {
      const product = action.payload;

      localStorage.setItem('cart', JSON.stringify(prevState.products.filter((prevProduct) => {
        return prevProduct.id !== product.id;
      })))

      return {
        ...prevState,
        products: prevState.products.filter((prevProduct) => {
          return prevProduct.id !== product.id;
        })
      };
    }
  },
});


// Экспортируем наружу все действия
export const { addProduct, removeProduct } = cartSlice.actions;

// И сам редуктор тоже
export default cartSlice.reducer;