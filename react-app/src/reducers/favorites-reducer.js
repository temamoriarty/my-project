import { createSlice } from "@reduxjs/toolkit";

const initialState = { products: localStorage.getItem('favorites') ? JSON.parse(localStorage.getItem('favorites')) : [] }

export const favoritesSlice = createSlice({
  name: "favorites",

  // Начальное состояние хранилища, товаров нет
  initialState: initialState,

  // Все доступные метода
  reducers: {
    // Добавить товар, первый параметр это текущее состояние
    // А второй параметр имеет данные для действия
    addToFavorites: (prevState, action) => {
      const product = action.payload;
      const hasInFavorites = prevState.products.some(
        (prevProduct) => prevProduct.id === product.id
      );

      if (hasInFavorites) return prevState;

      localStorage.setItem('favorites', JSON.stringify([...prevState.products, product]))

      return {
        ...prevState,
        // Внутри action.payload информация о добавленном товаре
        // Возвращаем новый массив товаров вместе с добавленным
        products: [...prevState.products, product],
      };
    },

    removeFromFavorites: (prevState, action) => {
      const product = action.payload;

      localStorage.setItem('favorites', JSON.stringify(prevState.products.filter((prevProduct) => {
        return prevProduct.id !== product.id;
      })))

      return {
        ...prevState,
        products: prevState.products.filter((prevProduct) => {
          return prevProduct.id !== product.id;
        })
      };
    }
  },
});


// Экспортируем наружу все действия
export const { addToFavorites, removeFromFavorites } = favoritesSlice.actions;

// И сам редуктор тоже
export default favoritesSlice.reducer;