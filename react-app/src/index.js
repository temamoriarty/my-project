import { createRoot } from "react-dom/client";
import { Provider } from "react-redux";
import { store } from "./store";
import App from "./App";
import { StrictMode } from "react";

const rootElement = document.getElementById("root");
const root = createRoot(rootElement);

root.render(
  /* Мы обернули наше приложение в провайдер
    чтобы оно имело доступ к хранилищу */
  <StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </StrictMode>
);
