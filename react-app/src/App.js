import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import PageProduct from './components/PageProduct/PageProduct';
import PageIndex from './components/PageIndex/PageIndex';
import './App.css';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<PageIndex />} />
        <Route path='/product' element={<PageProduct />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
