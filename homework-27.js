"use strict"

// Упражнение 1
// 1 способ.
// let time = +prompt("Введите число для обратного отсчета", 10);
// let int = Number.isInteger(time);
// if (!int || time < 0 || time === 0) {
//   console.log('Ошибка!');
// } else {
//   console.log('Осталось: ', time);
//   let intervalId = setInterval(() => {
//     time--;
//     console.log('Осталось: ', time);

//     if (time === 0) {
//       clearInterval(intervalId);
//       console.log('Время вышло!');
//     }
//   }, 1000);
// }

// Promise.

// let time = +prompt("Введите число для обратного отсчета", 10);
// let int = Number.isInteger(time);
// let promise = new Promise(function (resolve, reject) {
//   if (!int || time <= 0) {
//     reject(time)
//   } else {
//     console.log(`Стартовая точка отсчета: ${time}`);
//     resolve(time);
//   }
// });

// promise
//   .then(function () {
//     let intervalId = setInterval(() => {
//       time--;
//       console.log('Осталось: ', time);
//       if (time === 0) {
//         clearInterval(intervalId);
//         console.log('Время вышло!');
//       }
//     }, 1000);
//   })
//   .catch(function () {
//     console.error('Ошибка!');
//   });

// Упражнение 2
console.log('Упражнение 2:');

let promise1 = fetch('https://reqres.in/api/users');
const startTime = new Date().getTime();
promise1
  .then(function (response) {
    return response.json();
  })
  .then(function (response) {
    console.log(`Получили пользователей: ${response.data.length}`);
    response.data.forEach(function (user) {
      console.log(`— ${user.first_name} ${user.last_name} (${user.email})`);
    });
  })
  .catch(function () {
    console.error('Что-то пошло не так!')
  });
const endTime = new Date().getTime();
console.log(`Время выполнения запроса: ${end - start}ms`);