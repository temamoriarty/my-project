"use strict"

// Упражнение 1

let a = '$100';
let b = '300$';

let summ = +a.slice(1) + parseInt(b);
console.log(summ);

// Упражнение 2

let message = ' привет, медвед      ';

message = message.trim()[0].toUpperCase() + message.trim().slice(1);

console.log(message);

// Упражнение 3

let age = prompt('Сколько вам лет?');

if (age < 0 || age === null || age === '') {
  alert('Вы отменили действие или неправильно указали возраст');
} else if (age < 4) {
  alert(`Вам ${age} лет и вы младенец`);
} else if (age < 12) {
  alert(`Вам ${age} лет и вы ребенок`);
} else if (age < 19) {
  alert(`Вам ${age} лет и вы подросток`);
} else if (age < 41) {
  alert(`Вам ${age} лет и вы познаете жизнь`);
} else if (age < 81) {
  alert(`Вам ${age} лет и вы познали жизнь`);
} else if (age > 80) {
  alert(`Вам ${age} лет и вы долгожитель`);
} else {
  alert('Введите числовое значение');
}

// Упражнение 4

let text = 'Я работаю со строками, как профессионал!';

let count = text.split(' ').length;

console.log(count); // Вывод в консоль произойдет только после закрытия модального окна prompt