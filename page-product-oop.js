"use strict"

let form = document.querySelector('.form');

let inputNameContainer = form.querySelector('.add-opinion__name');
let inputRatingContainer = form.querySelector('.add-opinion__rating')

let inputName = inputNameContainer.querySelector('.input-name');
let inputNameError = inputNameContainer.querySelector('.error');

let inputRating = inputRatingContainer.querySelector('.input-rating');
let inputRatingError = inputRatingContainer.querySelector('.error');

let textArea = form.querySelector('.text-opinion');

// window.addEventListener('DOMContentLoaded', () => {
//   console.log('DOM fully loaded and parsed');

//   inputName.value = localStorage.getItem('name');
//   inputRating.value = localStorage.getItem('rating');
//   textArea.value = localStorage.getItem('opinion');
// });

class Form {
  constructor(errors) {
    this.errors = errors;
  }

  handleClick() {
    this._clearError(this.errors.inputNameError);
    this._clearError(this.errors.inputRatingError);
  }

  _clearError(elem) {
    elem.value = '';
    elem.classList.remove('error_visiable');
    inputName.classList.remove("input-error");
    inputRating.classList.remove("input-error");
  }
}

class AddReviewForm extends Form {
  constructor(inputs, storage, errors) {
    super(errors);
    this.inputs = inputs;
    this.storage = storage;
  }

  restoreData() {
    const obj = Object.entries(this.storage);

    obj.forEach((elem) => {
      const key = elem[0];
      const value = elem[1];

      if (key && key === 'name') {
        this.inputs.inputName.value = value;
      }

      if (key && key === 'rating') {
        this.inputs.inputRating.value = value;
      }

      if (key && key === 'opinion') {
        this.inputs.textArea.value = value;
      }
    });
  }

  handleInput(e) {
    localStorage.setItem(e.target.name, e.target.value);
  }

  _validate(type, value) {
    switch (type) {
      case 'name':
        if (value.length === 0) {
          return {
            text: 'Вы забыли указать имя и фамилию',
            isValid: false,
          };
        } else if (value.length < 2) {
          return {
            text: 'Имя не может быть короче 2-х символов',
            isValid: false,
          }
        } else return {
          text: '',
          isValid: true,
        }
      case 'rating':
        if (value.length === 0 || +value < 1 || +value > 5 || isNaN(+value)) {
          return {
            text: 'Оценка должна быть от 1 до 5',
            isValid: false,
          }
        } else return {
          text: '',
          isValid: true,
        }
    }
  }

  handleSubmit(e) {
    e.preventDefault();
    const name = this.inputs.inputName;
    const rating = this.inputs.inputRating;
    const textArea = this.inputs.textArea;
    const nameError = this._validate('name', name.value);
    const ratingError = this._validate('rating', rating.value);

    if (!nameError.isValid) {
      this.errors.inputNameError.classList.add("error_visiable");
      name.classList.add("input-error");
      this.errors.inputNameError.innerHTML = nameError.text;
      return;
    }
    if (!ratingError.isValid) {
      this.errors.inputRatingError.classList.add("error_visiable");
      rating.classList.add("input-error");
      this.errors.inputRatingError.innerHTML = ratingError.text;
    }
    if (nameError.isValid && ratingError.isValid) {
      this.storage.clear();
      name.value = '';
      rating.value = '';
      textArea.value = '';
    }
  }

  handleStorage() {
    this.inputs.inputName.value = localStorage.getItem('name');
    this.inputs.inputRating.value = localStorage.getItem('rating');
    this.inputs.textArea.value = localStorage.getItem('opinion');
  }
}

const inputsObj = {
  inputName,
  inputRating,
  textArea,
};

const errorsObj = {
  inputNameError,
  inputRatingError
}


const addReviewForm = new AddReviewForm(inputsObj, window.localStorage, errorsObj);
addReviewForm.restoreData();
form.addEventListener('input', addReviewForm.handleInput.bind(addReviewForm));
form.addEventListener('click', addReviewForm.handleClick.bind(addReviewForm));
form.addEventListener('submit', addReviewForm.handleSubmit.bind(addReviewForm));
window.addEventListener('storage', addReviewForm.handleStorage.bind(addReviewForm));
