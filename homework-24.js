"use strict"

// Упражнение 1
console.log('Упражнение 1:')
for (let i = 1; i < 21; i++) {
  if (i % 2 === 0) {
    console.log(i);
  }
}

// Упражнение 2

function showSum() {
  let sum = 0;
  for (let i = 0; i < 3; i++) {
    let number = +prompt('Введите число', '');
    if (!number) {
      alert('Ошибка, вы ввели не число!');
      break;
    }
    sum += number;
  }
  alert('Сумма: ' + sum);
}

// Упражнение 3
let month;
let result;
function getNameOfMonth(month) {
  switch (month) {
    case 0:
      result = 'Январь';
      break;
    case 1:
      result = 'Февраль';
      break;
    case 2:
      result = 'Март';
      break;
    case 3:
      result = 'Апрель';
      break;
    case 4:
      result = 'Май';
      break;
    case 5:
      result = 'Июнь';
      break;
    case 6:
      result = 'Июль';
      break;
    case 7:
      result = 'Август';
      break;
    case 8:
      result = 'Сентябрь';
      break;
    case 9:
      result = 'Октябрь';
      break;
    case 10:
      result = 'Ноябрь';
      break;
    case 11:
      result = 'Декабрь';
      break;
  }
  return result;
}
getNameOfMonth(9);
let nameMonth = result;
alert(nameMonth);

for (month = 0; month < 12; month++) {
  if (month === 9) continue;
  let nameMonth = getNameOfMonth(month);
  console.log(nameMonth);
}


// Упражнение 4

// Immediately Invoked Function Expression, IIFE — это функция, которая выполняется сразу же после того, как была определена.
// ; (function () {
//   // ...Тело функции
// })() Скобки вокруг превращают функцию в выражение, а () после вызывают выражение
// IIFE используют, когда не хотят перезаписывать уже заданную переменную из чужих модулей.