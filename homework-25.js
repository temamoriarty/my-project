"use strict"

// Упражнение 1

/**
 * Проверяет пустой ли объект
 * @param {Object} obj Имя объекта
 * @return {Boolean} Возвращает true, если obj пустой, иначе false
 */
function isEmpty(obj) {
  for (let key in obj) {
    return false;
  }
  return true;
}
/* let user = {};
alert(isEmpty(user)); // true
user.name = 'Artem';
alert(isEmpty(user)); // false
*/

// Упражнение 2 в data.js

// Упражнение 3

let salaries = {
  John: 100000,
  Ann: 160000,
  Pete: 130000,
};

/**
 * Возвращает salaries обновленную базу зарплат
 * @param {Number} perzent Процент, на который повышается зарплата
 * @return {Object} salaries после повышения зарплат на perzent
 */
function raiseSalary(perzent) {
  let sum = 0;
  for (let key in salaries) {
    salaries[key] = Math.floor(salaries[key] * (1 + perzent / 100));
    sum += salaries[key];
  }
  console.log(sum);
  return salaries;
}
raiseSalary(100);